namespace Poulp {
    /**
    * The errors that can be encountered by poulp.
    */
    public errordomain PoulpError {
        INVALID_MANIFEST,
        NO_MANIFEST,
        REPO_ERROR,
        PACKAGE_NOT_FOUND
    }

    public errordomain BuildError {
        INVALID_VALA_VERSION
    }

    /**
    * Recursively list the content of a directory.
    *
    * @param dir The directory to explore
    * @return A {@link GLib.string} array, containing the path of the files found, relatives to the base directory
    */
    public string[] recursive_ls (string dir, bool list_dirs = false, bool git = false) throws GLib.Error {
        string[] res = {};
        try {
            var src_file = File.new_for_path (dir);
            GLib.FileType src_type = src_file.query_file_type (GLib.FileQueryInfoFlags.NONE);
            if (src_type == GLib.FileType.DIRECTORY && (!src_file.get_path ().contains ("/.git") || git)) { // we explore all the directories, excepted .git
                var enumerator = src_file.enumerate_children (GLib.FileAttribute.STANDARD_NAME, GLib.FileQueryInfoFlags.NONE);
                for (GLib.FileInfo? info = enumerator.next_file (); info != null ; info = enumerator.next_file ()) {
                    foreach (string child in recursive_ls (GLib.Path.build_filename (dir, info.get_name ()), list_dirs, git)) {
                        res += child;
                    }
                }
                if (list_dirs) {
                    res += dir;
                }
            } else if (src_type == GLib.FileType.REGULAR ) { // we add files to the result
                res += src_file.get_path ();
            }
            return res;
        } catch (Error err) {
            Logger.log_error (err.message);
            return {};
        }
    }

    /**
    * Gives the poulp directory.
    *
    * This directory contains all the configuration and data files.
    *
    * On Windows, it will normally be `C:\ProgramData\poulp`. On other systems, `/etc/poulp/`.
    *
    * @return The poulp directory
    */
    public string get_poulp_dir () {
        return Environment.get_variable ("POULP_DATA_DIR");
    }

    /**
    * Gives the installed Vala version.
    */
    public SemVer.Version get_vala_version () {
        string raw_ver = "0.0.0";
        spawn ("valac --version", out raw_ver, null, null);
        raw_ver = raw_ver.replace ("Vala ", "").replace ("\n", "");
        return new SemVer.Version.parse (raw_ver);
    }

    /**
    * Spawns a process.
    *
    * It also sets the PROJECT_* environment variables before spawing the process.
    *
    * It uses bash or PowerShell (depending of the system) to allow command like `valac *.vala` to be spawned.
    *
    * @param _cmd The command to spawn.
    * @param stdout A string on which to write the standard output of the command.
    * @param stderr A string on which to write the standard error output of the command.
    * @param exit_code The exit code of the command.
    * @deprecated 0.1.0
    */
    [Version (deprecated = true, deprecated_since = "0.1.0", replacement = "spawn_async")]
    public void spawn (string _cmd, out string stdout, out string stderr, out int exit_code) {
        try {
            string cmd = _cmd;
            Manifest man = Manifest.get_for_current_project ();
            // wet some useful environment variables before running anything
            Environment.set_variable ("PROJECT_NAME", man.name, true);
            Environment.set_variable ("PROJECT_VERSION", man.version.to_string (), true);
            Environment.set_variable ("PROJECT_DESCRIPTION", man.description == null ? "" : man.description, true);
            Environment.set_variable ("PROJECT_LICENSE", man.license == null ? "" : man.license, true);
            Environment.set_variable ("PROJECT_HOMEPAGE", man.homepage == null ? "" : man.homepage, true);
            string output = man.output != null ? man.output : man.name + "-" + man.version.to_string ();
            Environment.set_variable ("PROJECT_OUTPUT", output, true);
            // use PowerShell/bach to make it possible to use wildcards and Co.
#if WIN32
            cmd =  "start powershell -NoExit -Command \"" + cmd.replace ("\"", "\\\"") + "\"";
#else
            cmd = "/bin/bash -c \"" + cmd.replace ("\"", "\\\"") + "\"";
#endif
            Process.spawn_command_line_sync (cmd, out stdout, out stderr, out exit_code);
            // unset the variables
            Environment.unset_variable ("PROJECT_NAME");
            Environment.unset_variable ("PROJECT_VERSION");
            Environment.unset_variable ("PROJECT_DESCRIPTION");
            Environment.unset_variable ("PROJECT_LICENSE");
            Environment.unset_variable ("PROJECT_HOMEPAGE");
            Environment.unset_variable ("PROJECT_OUTPUT");
        } catch {
            assert_not_reached ();
        }
    }

    public async int spawn_async (string cmd, string? dir = null) {
        try {
            Manifest man = Manifest.get_for_current_project ();
            // wet some useful environment variables before running anything
            Environment.set_variable ("PROJECT_NAME", man.name, true);
            Environment.set_variable ("PROJECT_VERSION", man.version.to_string (), true);
            Environment.set_variable ("PROJECT_DESCRIPTION", man.description == null ? "" : man.description, true);
            Environment.set_variable ("PROJECT_LICENSE", man.license == null ? "" : man.license, true);
            Environment.set_variable ("PROJECT_HOMEPAGE", man.homepage == null ? "" : man.homepage, true);
            Environment.set_variable ("PROJECT_OUTPUT", man.output == null ? man.name + "-" + man.version.to_string () : man.output, true);

            // use PowerShell/bach to make it possible to use wildcards and Co.
#if WIN32
            string[] shell = { "start", "powershell", "-NoExit", "-Command", cmd };
#else
            string[] shell = { "/bin/bash", "-c", cmd };
#endif
            Pid pid;
            Process.spawn_async_with_pipes (dir,
                                    shell,
                                    null,
                                    SpawnFlags.SEARCH_PATH | SpawnFlags.CHILD_INHERITS_STDIN | SpawnFlags.DO_NOT_REAP_CHILD,
                                    null,
                                    out pid);
            int exit_code = -1;
            ChildWatch.add (pid, (_pid, status) => {
                Process.close_pid (_pid);
                exit_code = status;
                spawn_async.callback ();
            });
            yield;
            // unset the variables
            Environment.unset_variable ("PROJECT_NAME");
            Environment.unset_variable ("PROJECT_VERSION");
            Environment.unset_variable ("PROJECT_DESCRIPTION");
            Environment.unset_variable ("PROJECT_LICENSE");
            Environment.unset_variable ("PROJECT_HOMEPAGE");
            Environment.unset_variable ("PROJECT_OUTPUT");

            return exit_code;
        } catch (Error err) {
            Logger.log_error (err.message);
        }
        return -1;
    }

    /**
    * Get the contents of a distant file.
    *
    * @param url The URL of the file to download.
    * @return A {@link GLib.uint8} array, containing the bytes of the responses body.
    */
    public uint8[] read_distant_file (string url) throws PoulpError {
        try {
            var url_re = new Regex ("^((http[s]?):\\/\\/)?\\/?([^:\\/\\s]+)((\\/\\w+)*\\/)([\\w\\-\\.]+[^#?\\s]+)(.*)?(#[\\w\\-]+)?$");
            MatchInfo info;

            if (url_re.match (url, 0, out info)) {
                // Get useful parts of the URL
                string host = info.fetch (3),
                path = info.fetch (4) + info.fetch (6);

                // Connect
                var client = new SocketClient ();
                client.tls = true;
                var conn = client.connect_to_uri (url, 443);

                // Send HTTP GET request
                var message = @"GET $path HTTP/1.1\r\nHost: $host\r\nAccept: */*\r\n\r\n";
                conn.output_stream.write (message.data);

                // Receive response
                var response = new DataInputStream (conn.input_stream);
                string line;
                bool first_line = true;
                int content_length = 0;
                while ((line = response.read_line (null)) != null) {
                    if (first_line && line != "HTTP/1.1 200 OK\r") {
                        throw new PoulpError.REPO_ERROR (@"Got %s when downloading $url".printf (line.replace ("\r", "")));
                    }
                    if (first_line) {
                        first_line = false;
                    }
                    if (line.has_prefix ("Content-Length: ")) {
                        content_length = int.parse (line.replace ("Content-Length: ", ""));
                    }
                    if (line == "\r") {
                        break;
                    }
                }
                uint8[] res = new uint8[content_length];
                response.read (res, null);
                return res;
            } else {
                Logger.log_error ("Invalid URL: %s", url);
            }
        } catch (PoulpError poulp_err) {
            throw poulp_err;
        } catch (Error e) {
            stderr.printf ("%s\n", e.message);
        }
        return new uint8 [] {};
    }

    /**
    * Downloads a file, async
    */
    public async uint8[] download (string url) {
        Soup.Session session = new Soup.Session ();
        Soup.Message msg = new Soup.Message ("GET", url);
        var res = new uint8[] {};
    	session.queue_message (msg, (sess, mess) => {
    		// Process the result:
    		res = mess.response_body.data;
            download.callback ();
    	});
        yield;
        return res;
    }


    public void setup_env () {
        Environment.set_variable ("VALA_VERSION", get_vala_version ().to_string (), false);
        Environment.set_variable ("VALA_API_VERSION", string.joinv (".", get_vala_version ().to_string ().split (".")[0:2]), false);
#if WIN32
        Environment.set_variable ("POULP_DATA_DIR", "C:\\ProgramData\\poulp", false);
        Environment.set_variable ("POULP_BIN_DIR", "C:\\ProgramFiles\\", false);
        Environment.set_variable ("POULP_HEADERS_DIR", "C:\\vala-0.20.1\\include", false);
        Environment.set_variable ("POULP_LIB_DIR", "C:\\vala-0.20.1\\lib", false);
        Environment.set_variable ("POULP_VAPI_DIR", "C:\\ProgramData\\vala-0.20.1\\vapi", false);
        Environment.set_variable ("POULP_PKG_DIR", "C:\\vala-0.20.1\\pkgconfig", false);
#else
        Environment.set_variable ("POULP_DATA_DIR", "/etc/poulp/", false);
        Environment.set_variable ("POULP_BIN_DIR", "/usr/bin/", false);
        Environment.set_variable ("POULP_HEADERS_DIR", "/usr/include/", false);
        Environment.set_variable ("POULP_LIB_DIR", "/usr/lib64/", false);
        Environment.set_variable ("POULP_VAPI_DIR", "/usr/share/vala/vapi", false);
        Environment.set_variable ("POULP_PKG_DIR", "/usr/lib64/pkgconfig/", false);
#endif
    }

    /**
    * Setup the default configuration and data files
    */
    public bool create_directories (bool force) {
        const string default_repos = "https://gitlab.com/Bat41/poulp-repo-test/raw/master/dist\n";
        const string default_projects = """[
{
    "id": "console",
    "name": "Console",
    "description": "A very basic console application",
    "files": [ "main.vala" ]
}
]""";
        const string console = """
int main () {
print ("Hello, world!");
return 0;
}
""";

        try {
            File poulp_dir = File.new_for_path (get_poulp_dir ());
            if (!poulp_dir.query_exists () || force) {
                poulp_dir.make_directory ();
                FileUtils.set_contents (Path.build_filename (get_poulp_dir (), "repos"), default_repos);

                DirUtils.create_with_parents (Path.build_filename (get_poulp_dir (), "files"), 0777);
                FileUtils.set_contents (Path.build_filename (get_poulp_dir (), "files"), "[]");

                DirUtils.create_with_parents (Path.build_filename (get_poulp_dir (), "projects"), 0777);
                FileUtils.set_contents (Path.build_filename (get_poulp_dir (), "projects", "projects.json"), default_projects);
                DirUtils.create_with_parents (Path.build_filename (get_poulp_dir (), "projects", "console"), 0777);
                FileUtils.set_contents (Path.build_filename (get_poulp_dir (), "projects", "console", "main.vala"), console);
            }
            return true;
        } catch (Error err) {
            Logger.log_error ("Failed to create necessary directories. Try to run the command as an administrator.");
            return false;
        }
    }
}
