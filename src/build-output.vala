using Gee;

namespace Poulp {

    /**
    * Represents the result of a build.
    */
    public abstract class BuildOutput : Object {

        /**
        * false if build failed, true otherwise
        */
        public bool success { get; set; default = true; }

        /**
        * The absolute path to the projects root
        */
        public string project_path { get; set; }

        /**
        * The directory where the output can be found (`build` by default)
        */
        public string build_dir { get; set; }

        /**
        * The name of the output
        */
        public string name { get; set; }

        /**
        * The bsolute path to the output
        */
        public string output_path {
            owned get {
                return Path.build_filename (this.project_path, this.build_dir, this.name);
            }
        }

        /**
        * Sets the output files for a project
        *
        * @param name The name of the project
        */
        public abstract void set_output (string name);

        /**
        * Installs the output file(s)
        */
        public abstract async bool install ();
    }
}
