using Gee;
using SemVer;

namespace Poulp {

    /**
    * Installs packages
    */
    public class Installer : Object {

        /**
        * A list of repositories where we search packages
        */
        public ArrayList<Repository> repositories { get; set; default = new ArrayList<Repository> (); }

        /**
        * Emitted when a package isn't found in any remote repository
        */
        public signal void not_found (string pkg);

        /**
        * Installs a package
        *
        * @param pkg The package to install
        * @param range A {@link SemVer.Range} that should contain a version of the package
        */
        public async bool install (string pkg, Range range) {
            foreach (var repo in this.repositories) {
                if (repo.packages.has_key (pkg)) {
                    try {
                        print ("Cloning %s...\n", pkg);

                        string clone_dir = Path.build_filename (Environment.get_tmp_dir (), "poulp", pkg);
                        File dir = File.new_for_path (clone_dir);
                        // removing files if they are already here
                        if (dir.query_exists ()) {
                            foreach (string file in recursive_ls (dir.get_path (), true, true)) {
                                File child = File.new_for_path (file);
                                child.delete ();
                            }
                        } else {
                            dir.make_directory_with_parents ();
                        }

                        int exit_code = yield spawn_async ("git clone %s %s -q".printf (repo.packages[pkg], clone_dir));
                        if (exit_code != 0) {
                            Logger.log_error ("Can't clone %s", pkg);
                            return false;
                        }

                        print ("Building %s\n", pkg);

                        string tags = "";
                        Process.spawn_sync (clone_dir, { "git", "tag" }, null, SpawnFlags.SEARCH_PATH, null, out tags);
                        SemVer.Version ver = new SemVer.Version (0, 0, 0);
                        foreach (string tag in tags.split ("\n")) {
                            if (valid (tag)) {
                                var v = new SemVer.Version.parse (tag);
                                if (v in range && v.gt (ver)) {
                                    ver = v;
                                }
                            }
                        }
                        if (ver.eq (new SemVer.Version (0, 0, 0))) {
                            Logger.log_error ("No matching version of %s found.", pkg);
                            return false;
                        }
                        yield spawn_async (@"git checkout $ver -q", clone_dir);
                        Builder bld = new Builder (new Manifest.for_path (Path.build_filename (clone_dir, "poulp.toml")));
                        BuildOutput build = yield bld.build (clone_dir);
                        yield build.install ();
                    } catch (Error err) {
                        Logger.log_error (err.message);
                    }
                }
            }
            return true;
        }
    }
}
