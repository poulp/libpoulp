using Toml;
using SemVer;
using Gee;

namespace Poulp {
    /**
    * Represents the manifest of a project
    *
    * You can use it to read and modify poulp.toml files.
    */
    public class Manifest : Object {

        /**
        * Gives you the manifest for the current project
        */
        public static Manifest get_for_current_project () throws PoulpError {
            try {
                return new Manifest.for_path (Path.build_filename (Environment.get_current_dir (), "poulp.toml"));
            } catch (PoulpError err) {
                throw err;
            }
        }

        /**
        * Create a new manifest that you can fully customize
        *
        * @param name The name of the new project
        */
        public Manifest.custom (string name) {
            this.name = name;
        }

        /**
        * Parses the manifest at the given path
        *
        * @param path The path to the manifest to read.
        */
        public Manifest.for_path (string path) throws PoulpError {
            try {
                this.from_toml (new Parser.from_path (path).parse ());
            } catch (PoulpError perr) {
                throw perr;
            } catch (TomlError terr) {
                Logger.log_error (terr.message);
            } catch (Error err) {
                throw new PoulpError.NO_MANIFEST ("Can't read %s, the project probably doesn't contain a `poulp.toml` file.\nDetails: %s".printf (path, err.message));
            }
        }

        /**
        * Parses a manifest from a raw TOML string.
        */
        public Manifest (string manifest) throws PoulpError, Error {
            try {
                this.from_toml (new Parser (manifest).parse ());
            } catch (TomlError terr) {
                Logger.log_error (terr.message);
            }
        }

        /**
        * Creates a manifest from a {@link Toml.Element}
        *
        * @param elt The {@link Toml.Element} that contains the parsed manifest
        */
        public Manifest.from_toml (Element elt) throws PoulpError, TomlError {
            this.toml = elt;
            // if there is no "name" property in the manifest, throw an error
            this.name = "name" in elt ? elt["name"].as<string> () : null;
            if (this.name == null) {
                throw new PoulpError.INVALID_MANIFEST ("The `name` key in the manifest is not optional.");
            }

            string v = "version" in elt ? elt["version"].as<string> () : null;
            if (v == null) {
                throw new PoulpError.INVALID_MANIFEST ("The `version` key in the manifest is not optional.");
            }
            if (!SemVer.valid (v)) {
                throw new PoulpError.INVALID_MANIFEST ("Use semantic versionning (e.g X.Y.Z)");
            }
            this.version = new SemVer.Version.parse (v);

            // parsing properties from the manifest
            // we don't use the deserializer provided by toml-glib,
            // because of ArrayList that are not supported yet (but it's planned).
            this.description = "description" in elt ? elt["description"].as<string> () : null;
            if ("keywords" in elt) {
                foreach (var kw in elt["keywords"].as<ArrayList<Element>> ()) {
                    if (kw.value_type != typeof (string)) {
                        Logger.log_warning ("Manifest contains an invalid value in `keywords`.");
                    }
                    this.keywords.add (kw.as<string> ());
                }
            }

            if ("features" in elt) {
                foreach (var feat in elt["features"].as<ArrayList<Element>> ()) {
                    this.features[feat.identifier] = feat.as<bool> ();
                }
            }

            if ("bugs" in elt) {
                if ("url" in elt["bugs"]) {
                    this.bug_url = elt["bugs"]["url"].as<string> ();
                }
                if ("email" in elt["bugs"]) {
                    this.bug_email = elt["bugs"]["url"].as<string> ();
                }
            }
            this.homepage = "homepage" in elt ? elt["homepage"].as<string> () : null;
            this.license = "license" in elt ? elt["license"].as<string> () : null;
            if ("authors" in elt) {
                foreach (var author in elt["authors"].as<ArrayList<Element>> ()) {
                    if (author.value_type != typeof (string)) {
                        Logger.log_warning ("Manifest contains an invalid value in `authors`.");
                        continue;
                    }
                    this.authors.add (author.as<string> ());
                }
            }

            if ("files" in elt) {
                foreach (var file in elt["files"].as<ArrayList<Element>> ()) {
                    if (file.value_type != typeof (string)) {
                        Logger.log_warning ("Manifest contains an invalid value in `files`.");
                        continue;
                    }
                    this.files.add (file.as<string> ());
                }
            }
            this.repository = "repository" in elt ? elt["repository"].as<string> () : null;
            this.vapi_dir = "vapi-dir" in elt ? elt["vapi-dir"].as<string> () : null;
            this.output = "output" in elt ? elt["output"].as<string> () : null;
            if ("scripts" in elt) {
                foreach (var script in elt["scripts"].as<ArrayList<Element>> ()) {
                    ArrayList<string> cmds = new ArrayList<string> ();
                    var commands = elt["scripts"][script.identifier].as<ArrayList<Element>> ();
                    foreach (var cmd in commands) {
                        if (cmd.value_type != typeof (string)) {
                            Logger.log_warning ("Manifest contains an invalid value in `scripts.%s`.".printf (script.identifier));
                            continue;
                        }
                        cmds.add (cmd.as<string> ());
                    }
                    this.scripts[script.identifier] = cmds;
                }
            }
            this.library = "library" in elt ? elt["library"].as<bool> () : false;
            if ("dependencies" in elt) {
                foreach (var dep in elt["dependencies"].as<ArrayList<Element>> ()) {
                    if (dep.value_type != typeof (string)) {
                        Logger.log_error ("%s: Version should be a string.", dep.identifier);
                        continue;
                    }
                    string raw_version = dep.as<string> ();
                    this.dependencies[dep.identifier] = new Range (raw_version);
                }
            }
            if ("dev-dependencies" in elt) {
                foreach (var dep in elt["dev-dependencies"].as<ArrayList<Element>> ()) {
                    if (dep.value_type != typeof (string)) {
                        Logger.log_error ("%s: Version should be a string.", dep.identifier);
                        continue;
                    }
                    string raw_version = dep.as<string> ();
                    this.dev_dependencies[dep.identifier] = new Range (raw_version);
                }
            }
            this.vala_version = "vala-version" in elt ? new Range (elt["vala-version"].as<string> ()) : null;
            if ("compiler-params" in elt) {
                foreach (var param in elt["compiler-params"].as<ArrayList<Element>> ()) {
                    if (param.value_type != typeof (string)) {
                        Logger.log_warning ("Manifest contains an invalid value in `compiler-params`.");
                        continue;
                    }
                    this.compiler_params.add (param.as<string> ());
                }
            }

            // assert that libraries always have a `repository` field.
            if (this.library && this.repository == null) {
                Logger.log_warning ("Your library doesn't have any Git repository associated with it.\nYou won't be able to publish it.\nFill the `repository' field of your `poulp.toml'.");
            }
        }

        /**
        * Creates a {@link Toml.Element} from the manifest
        *
        * @return The {@link Toml.Element} representing this manifest.
        */
        public Element to_toml () {
            // we update this.toml before returning it

            try {
                toml["name"] = new Element (this.name);

                if (this.version != null) {
                    toml["version"] = new Element (this.version.to_string ());
                }

                if (this.description != null) {
                    toml["description"] = new Element (this.description);
                }

                if (this.keywords.size > 0) {
                    toml["keywords"] = new Element.array ();
                    foreach (string kw in this.keywords) {
                        toml["keywords"].as<ArrayList<Element>> ().add (new Element (kw));
                    }
                }

                if (this.bug_email != null || this.bug_url != null) {
                    toml["bugs"] = new Element.table ();
                    if (this.bug_url != null) {
                        toml["bugs"]["url"] = new Element (this.bug_url);
                    }
                    if (this.bug_email != null) {
                        toml["bugs"]["email"] = new Element (this.bug_email);
                    }
                }

                if (this.homepage != null) {
                    toml["homepage"] = new Element (this.homepage);
                }

                if (this.license != null) {
                    toml["license"] = new Element (this.license);
                }

                if (this.authors.size > 0) {
                    toml["authors"] = new Element.array ();
                    foreach (string author in this.authors) {
                        toml["authors"].as<ArrayList<Element>> ().add (new Element (author));
                    }
                }

                if (this.files.size > 0) {
                    toml["files"] = new Element.array ();
                    foreach (string file in this.files) {
                        toml["files"].as<ArrayList<Element>> ().add (new Element (file));
                    }
                }

                if (this.repository != null) {
                    toml["repository"] = new Element (this.repository);
                }

                if (this.vapi_dir != null) {
                    toml["vapi-dir"] = new Element (this.vapi_dir);
                }

                if (this.output != null) {
                    toml["output"] = new Element (this.output);
                }

                if (this.scripts.size > 0) {
                    toml["scripts"] = new Element.table ();
                    foreach (var entry in this.scripts.entries) {
                        var cmds = new ArrayList<Element> ();
                        foreach (string cmd in entry.value) {
                            cmds.add (new Element (cmd));
                        }
                        toml["scripts"][entry.key] = new Element.array (cmds);
                    }
                }

                if (this.library) {
                    toml["library"] = new Element (this.library);
                }

                if (this.dependencies.size > 0) {
                    toml["dependencies"] = new Element.table ();
                    foreach (var dep in this.dependencies.entries) {
                        toml["dependencies"][dep.key] = new Element (dep.value.to_string ());
                    }
                }

                if (this.dev_dependencies.size > 0) {
                    toml["dev-dependencies"] = new Element.table ();
                    foreach (var dep in this.dev_dependencies.entries) {
                        toml["dev-dependencies"][dep.key] = new Element (dep.value.to_string ());
                    }
                }

                if (this.vala_version != null) {
                    toml["vala-version"] = new Element (this.vala_version.to_string ());
                }

                if (this.compiler_params.size > 0) {
                    toml["compiler-params"] = new Element.array ();
                    foreach (string param in this.compiler_params) {
                        toml["compiler-params"].as<ArrayList<Element>> ().add (new Element (param));
                    }
                }
            } catch (Error err) {
                Logger.log_error (err.message);
            }

            return this.toml;
        }

        /**
        * Creates a raw TOML string representing this manifest.
        *
        * @return A {@link GLib.string} representation of this, in TOML.
        */
        public string to_toml_string () {
            Writer wr = new Writer ();
            return wr.write (this.toml);
        }

        /**
        * Add all the files that can be used for the project to this.files
        *
        * @param path The path where to start searching.
        */
        public void add_files (string path) {
            try {
                string[] files = recursive_ls (path);
                foreach (string file in files) {
                    if (file.has_suffix (".vala") || file.has_suffix (".gs") || file.has_suffix (".c")) {
                        this.files.add (file.replace (Environment.get_current_dir () + Path.DIR_SEPARATOR_S, "./"));
                    }
                }
            } catch (Error err) {
                Logger.logc (LogColor.BOLD_RED, "Cannot list source files, skipping.");
                return;
            }
        }

        private Element toml { get; set; default = new Element.table (); }

        /**
        * The name of the project.
        */
        public string name { get; set; }

        /**
        * The version of the project.
        */
        public SemVer.Version version { get; set; }

        /**
        * A short description of the project.
        */
        public string description { get; set; }

        /**
        * Some keywords related to this project.
        */
        public ArrayList<string> keywords { get; set; default = new ArrayList<string> (); }

        /**
        * The features of valac to enable/disable.
        */
        public HashMap<string, bool> features { get; set; default = new HashMap<string, bool> (); }

        /**
        * The URL where you can report bugs.
        */
        public string bug_url { get; set; }

        /**
        * The email to report bugs
        */
        public string bug_email { get; set; }

        /**
        * The project's homepage.
        */
        public string homepage { get; set; }

        /**
        * The license of the project.
        */
        public string license { get; set; }

        /**
        * A list of the people that created the project.
        */
        public ArrayList<string> authors { get; set; default = new ArrayList<string> (); }

        /**
        * The files of the project.
        */
        public ArrayList<string> files { get; set; default = new ArrayList<string> (); }

        /**
        * The URL where the source code can be found.
        */
        public string repository { get; set; }

        /**
        * The path to vapis directory.
        **/
        public string vapi_dir { get; set; }

        /**
        * A property to define custom output file.
        **/
        public string output { get; set; }

        /**
        * A boolean to say if the project is a library.
        */
        public bool library { get; set; default = false; }

        /**
        * The librairies to --pkg with this project.
        */
        public HashMap<string, Range> dependencies { get; set; default = new HashMap<string, Range> (); }

        /**
        * Same as {@link Poulp.Manifest.dependencies}, but only used when developping (testing framework and Co.)
        */
        public HashMap<string, Range> dev_dependencies { get; set; default = new HashMap<string, Range> (); }

        /**
        * The Vala version that is needed to build this project.
        */
        public Range vala_version { get; set; }

        /**
        * Extra parameters to pass to the compiler
        */
        public ArrayList<string> compiler_params { get; set; default = new ArrayList<string> (); }

        /**
        * The scripts that are available for this project.
        */
        public HashMap<string, ArrayList<string>> scripts { get; set; default = new HashMap<string, ArrayList<string>> (); }
    }
}
