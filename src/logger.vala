namespace Poulp {
    /**
    * Defines the allowed colors.
    */
    namespace LogColor {
        public const string NORMAL = "";
        public const string RESET = "\033[m";
        public const string BOLD = "\033[1m";
        public const string RED = "\033[31m";
        public const string GREEN = "\033[32m";
        public const string YELLOW = "\033[33m";
        public const string BLUE = "\033[34m";
        public const string MAGENTA = "\033[35m";
        public const string CYAN = "\033[36m";
        public const string BOLD_RED = "\033[1;31m";
        public const string BOLD_GREEN = "\033[1;32m";
        public const string BOLD_YELLOW = "\033[1;33m";
        public const string BOLD_BLUE = "\033[1;34m";
        public const string BOLD_MAGENTA = "\033[1;35m";
        public const string BOLD_CYAN = "\033[1;36m";
        public const string BG_RED = "\033[41m";
        public const string BG_GREEN = "\033[42m";
        public const string BG_YELLOW = "\033[43m";
        public const string BG_BLUE = "\033[44m";
        public const string BG_MAGENTA = "\033[45m";
        public const string BG_CYAN = "\033[46m";
    }

    /**
    * An utility to log beautiful and coherent messages.
    */
    public class Logger : Object {

        /**
        * This method prints a message with full colored line.
        *
        * @param color The color to apply to the message.
        * @param message The message to print.
        * @param args A va_list that'll be vsprintf'ed to message.
        */
        public static void logc (string color, string message, ...) {
            stdout.puts (color + message.vprintf (va_list ()) + LogColor.RESET + "\n");
        }

        /**
        * This method do the same as logc, but avoid adding a newline (\n) at the end of the message.
        *
        * @param color The color to apply to the message.
        * @param message The message to print.
        * @param args A va_list that'll be vsprintf'ed to message.
        */
        public static void logcc (string color, string message, ...) {
            stdout.puts (color + message.vprintf (va_list ()) + LogColor.RESET);
        }

        /**
        * This method prints a message.
        *
        * @param message The message to print.
        * @param args A va_list that'll be vsprintf'ed to message.
        */
        public static void log (string message, ...) {
            stdout.puts (message.vprintf (va_list ()) + "\n");
        }

        /**
        * Logs an error.
        *
        * @param error The message to print.
        * @param args A va_list that'll be vsprintf'ed to message.
        */
        public static void log_error (string error, ...) {
            Logger.logc (LogColor.RED, "[ERROR] " + error.vprintf (va_list ()));
        }

        /**
        * Logs a warning.
        *
        * @param warning The message to print.
        * @param args A va_list that'll be vsprintf'ed to message.
        */
        public static void log_warning (string warning, ...) {
            Logger.logc (LogColor.YELLOW, "[WARNING] " + warning.vprintf (va_list ()));
        }

        /**
        * Ask the user to enter an input.
        *
        * @param message The message to print.
        * @param args A va_list that'll be vsprintf'ed to message.
        */
        public static string ask (string message, ...) {
            Logger.logcc (LogColor.BOLD_CYAN, message, va_list ());
            string input = stdin.read_line ();

            if (input == "" || input == null) {
                return "";
            }

            return input;
        }

        /**
        * Ask the user to confirm or decline. [Y/n]
        *
        * @param message The message to print.
        * @param args A va_list that'll be vsprintf'ed to message.
        * @return true if user answered by Y, false if n.
        */
        public static bool askyn (string message, ...) {
            string input = "";
            bool is_valid_input = false;

            Logger.logcc (LogColor.BOLD_CYAN, message + " [Y/n] ", va_list ());

            // while the user didn't choosed between yes and no
            while (is_valid_input == false) {
                input = stdin.read_line ().down ();

                if (input == "y" || input == "n") {
                    is_valid_input = true;
                    break;
                } else {
                    Logger.logcc (LogColor.BOLD_YELLOW, "Invalid choice, please choose beetwen Y(es) or n(o). ");
                }
            }

            return (input == "y");
        }
    }
}
