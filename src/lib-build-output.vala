using Gee;

namespace Poulp {

    /**
    * The result of a library build
    */
    public class LibBuildOutput : BuildOutput {

        /**
        * The (base)name of the .vapi file
        */
        public string vapi_file { get; set; }

        /**
        * The (base)name of the .deps file
        */
        public string deps_file { get; set; }

        /**
        * The (base)name of the .h file
        */
        public string header_file { get; set; }

        /**
        * The (base)name of the .so/.dll file
        */
        public string lib_file { get; set; }

        /**
        * The (base)name of the .pc file
        */
        public string pc_file { get; set; }

        /**
        * {@inheritDoc}
        */
        public override void set_output (string name) {
            this.name = name;
            this.vapi_file = name + ".vapi";
            this.deps_file = name + ".deps";
            this.header_file = name + ".h";
#if WIN32
            string ext = ".exe";
#else
            string ext = ".so";
#endif

            this.lib_file = "lib" + name + ext;
            this.pc_file = name + ".pc";
        }

        /**
        * {@inheritDoc}
        */
        public override async bool install () {
            try {
                Manifest man = new Manifest.for_path (Path.build_filename (this.project_path, "poulp.toml"));

                if (man.scripts.has_key ("pre-install")) {
                    return yield new ScriptRunner ().run (man.scripts["pre-install"]);
                }

                if (man.scripts.has_key ("install")) {
                    return yield new ScriptRunner ().run (man.scripts["install"]);
                } else {
                    HashMap<string, string> dest = new HashMap<string, string> ();
                    dest["vapi"] = Path.build_filename (Environment.get_variable ("POULP_VAPI_DIR"), this.vapi_file);
                    dest["deps"] = Path.build_filename (Environment.get_variable ("POULP_VAPI_DIR"), this.deps_file);
                    dest["pc"] = Path.build_filename (Environment.get_variable ("POULP_PKG_DIR"), this.pc_file);
                    dest["lib"] = Path.build_filename (Environment.get_variable ("POULP_LIB_DIR"), this.lib_file);
                    dest["header"] = Path.build_filename (Environment.get_variable ("POULP_HEADERS_DIR"), this.name, this.header_file);

                    HashMap<string, File> files = new HashMap<string, File> ();
                    files["vapi"] = File.new_for_path (Path.build_filename (this.project_path, this.build_dir, this.vapi_file));
                    files["deps"] = File.new_for_path (Path.build_filename (this.project_path, this.build_dir, this.deps_file));
                    files["header"] = File.new_for_path (Path.build_filename (this.project_path, this.build_dir, this.header_file));
                    files["lib"] = File.new_for_path (Path.build_filename (this.project_path, this.build_dir, this.lib_file));
                    files["pc"] = File.new_for_path (Path.build_filename (this.project_path, this.build_dir, this.pc_file));

                    foreach (var entry in dest.entries) {
                        File dir = File.new_for_path (entry.value);
                        files[entry.key].copy (dir, FileCopyFlags.OVERWRITE | FileCopyFlags.ALL_METADATA);
                    }
                }

                if (man.scripts.has_key ("post-install")) {
                    return yield new ScriptRunner ().run (man.scripts["post-install"]);
                }

                return true;
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }
        }

    }
}
