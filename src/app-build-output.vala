namespace Poulp {

    /**
    * Represents the result of an application build
    */
    public class AppBuildOutput : BuildOutput {

        /**
        * The (base)name of the binary file
        */
        public string bin_file { get; set; }

        /**
        * {@inheritDoc}
        */
        public override void set_output (string name) {
            this.name = name;
#if WIN32
            this.bin_file = name + ".exe";
#else
            this.bin_file = name;
#endif
        }

        /**
        * {@inheritDoc}
        */
        public override async bool install () {
            try {
                Manifest man = new Manifest.for_path (Path.build_filename (this.project_path, "poulp.toml"));

                if (man.scripts.has_key ("pre-install")) {
                    return yield new ScriptRunner ().run (man.scripts["pre-install"]);
                }

                if (man.scripts.has_key ("install")) {
                    return yield new ScriptRunner ().run (man.scripts["install"]);
                } else {
                    string install_dir = Environment.get_variable ("POULP_BIN_DIR");
                    File bin = File.new_for_path (Path.build_filename (this.project_path, this.build_dir, this.bin_file));

                    File dir = File.new_for_path (Path.build_filename (install_dir, this.bin_file));
                    bin.copy (dir, FileCopyFlags.OVERWRITE | FileCopyFlags.ALL_METADATA);

                }

                if (man.scripts.has_key ("post-install")) {
                    return yield new ScriptRunner ().run (man.scripts["post-install"]);
                }

                return true;
            } catch (Error err) {
                Logger.log_error (err.message);
                return false;
            }
        }
    }
}
