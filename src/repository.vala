using Gee;

namespace Poulp {
    /**
    * A class to manage a remote repository.
    */
    public class Repository : Object {
        /**
        * A list of packages.
        *
        * Keys are packages names and values their Git repository URL.
        */
        public HashMap<string, string> packages { get; set; default = new HashMap<string, string> (); }

        /**
        * The name of the remote repository
        */
        public string name { get; set; }

        /**
        * Creates a new {@link Poulp.Repository} from a {@link Json.Object}
        *
        * @param root The packages.json of this repository
        */
        public Repository.from_json (Json.Object root) {
            this.name = root["name"].as<string> ();
            foreach (var pkg in root["packages"].as_object ().properties) {
                this.packages[pkg.name] = pkg.value.as<string> ();
            }
        }

        /**
        * Gives a list of all registred repositories
        */
        public static async ArrayList<Repository> load_known () throws Error {
            ArrayList<Repository> known = new ArrayList<Repository> ();
            string repo_list;
            try {
                FileUtils.get_contents (Path.build_filename (get_poulp_dir (), "repos"), out repo_list);
                foreach (string url in repo_list.split ("\n")) {
                    if (url.length != 0) {
                        var repo_content = yield download (Path.build_path ("/", url, "packages.json"));
                        Json.Parser parser = new Json.Parser ();
                        parser.load_from_data ((string)repo_content);
                        var repo = new Repository.from_json (parser.root.as_object ());
                        known.add (repo);
                    }
                }
            } catch (Error err) {
                throw err;
            }
            return known;
        }
    }
}
