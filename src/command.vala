namespace Poulp {
    /**
    * A command that can be run by poulp
    */
    public interface Command : Object {
        /**
        * Called when the command is ran by the user.
        *
        * @param args Extra arguments passed to the command
        */
        public abstract async bool run (string[] args);

        /**
        * Gives an help message about this command.
        *
        * @return The help message.
        */
        public abstract string help (bool is_short);
    }
}
