using Gee;

namespace Poulp {

    /**
    * Runs a script
    */
    public class ScriptRunner : Object {

        /**
        * The environment variables
        */
        public HashMap<string, string> env { get; set; default = new HashMap<string, string> (); }

        /**
        * The working directory for scripts commands
        */
        public string cwd { get; set; default = Environment.get_current_dir () ; }

        /**
        * Loads environment from a raw environment (e.g. `k=v` list)
        */
        public void load_env (string[] raw_env = GLib.Environ.get ()) {
            foreach (string variable in raw_env) {
                if (!("=" in variable)) {
                    continue;
                }

                string[] part = variable.split ("=");
                this.env[part[0]] = part[1];
            }
        }

        /**
        * Compiles environment into a `k=v` list
        */
        public string[] compile_env () {
            string[] res = {};

            foreach (var entry in this.env.entries) {
                res += "%s=%s".printf (entry.key, Shell.quote (entry.value));
            }

            return res;
        }

        /**
        * Runs a list of commands
        *
        * @param commands The commands to run
        */
        public async bool run (ArrayList<string> commands) {
            bool res = true;
            try {
                Manifest man = Manifest.get_for_current_project ();
                // wet some useful environment variables before running anything
                this.env["PROJECT_NAME"] = man.name;
                this.env["PROJECT_VERSION"] = man.version.to_string ();
                this.env["PROJECT_DESCRIPTION"] = man.description == null ? "" : man.description;
                this.env["PROJECT_LICENSE"] = man.license == null ? "" : man.license;
                this.env["PROJECT_HOMEPAGE"] = man.homepage == null ? "" : man.homepage;
                this.env["PROJECT_OUTPUT"] = man.output == null ? man.name + "-" + man.version.to_string () : man.output;

                foreach (string cmd in commands) {
                    // use PowerShell/bach to make it possible to use wildcards and Co.
#if WIN32
                    string[] shell = { "start", "powershell", "-NoExit", "-Command", Shell.quote (cmd) };
#else
                    string[] shell = { "/bin/bash", "-c", Shell.quote (cmd) };
#endif
                    Pid pid;
                    Process.spawn_async_with_pipes (this.cwd,
                                            shell,
                                            this.compile_env (),
                                            SpawnFlags.SEARCH_PATH | SpawnFlags.CHILD_INHERITS_STDIN | SpawnFlags.DO_NOT_REAP_CHILD,
                                            null,
                                            out pid);
                    int exit_code = 42;
                    ChildWatch.add (pid, (_pid, status) => {
                        Process.close_pid (_pid);
                        exit_code = status;
                        this.run.callback ();
                    });
                    yield;
                    res = res && (exit_code == 0);
                }
            } catch (Error err) {
                Logger.log_error (err.message);
                res = false;
            }
            return res;
        }
    }
}
